package com.csa.backend.zkpfr;

import com.csa.backend.zkpfr.repositories.ChannelRepository;
import com.csa.backend.zkpfr.repositories.PostRepository;
import com.csa.backend.zkpfr.repositories.StatusRepository;
import com.csa.backend.zkpfr.services.IUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ConfigurableApplicationContext;

@SpringBootApplication
public class ZkpfrApplication implements CommandLineRunner {

	public static void main(String[] args) {
		ConfigurableApplicationContext applicationContext = SpringApplication.run(ZkpfrApplication.class, args);
	}


	@Override
	public void run(String... args) throws Exception {

	}
}
