package com.csa.backend.zkpfr.controller;

import com.csa.backend.zkpfr.model.Channel;
import com.csa.backend.zkpfr.model.Post;
import com.csa.backend.zkpfr.services.IChannelService;
import com.csa.backend.zkpfr.services.IPostService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@RequestMapping(value = "/api/v1/channels")

public class ChannelController {

    private final IChannelService channelService;

    @Autowired
    public ChannelController(IChannelService channelService) {
        this.channelService = channelService;
    }

    @PostMapping
    public Channel addChannel(@RequestBody @Valid Channel channelToAdd) {
        return this.channelService.save(channelToAdd);
    }

    @PatchMapping
    public Channel updateChannel(@RequestBody @Valid Channel channelToAdd) {
        return this.channelService.save(channelToAdd);
    }

    @GetMapping
    public Iterable<Channel> getAllChannels() {
        return this.channelService.findAll();
    }

    @GetMapping(path = "{id}")
    public Channel getChannelById(@PathVariable("id") int id) {
        return this.channelService.findById(id)
            .orElse(null);
    }

    @DeleteMapping(path = "{id}")
    public void deletePostById(@PathVariable("id") int id) {
        this.channelService.deleteById(id);
    }
}
