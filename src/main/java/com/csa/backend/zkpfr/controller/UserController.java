package com.csa.backend.zkpfr.controller;

import com.csa.backend.zkpfr.model.User;
import com.csa.backend.zkpfr.services.IUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.lang.NonNull;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

// consumes = {"application/json", " "}

@RestController
@RequestMapping(value = "/api/v1/users")
public class UserController {

    private final IUserService userService;

    @Autowired
    public UserController(IUserService userService) {
        this.userService = userService;
    }

    @GetMapping
    public Iterable<User> getAllUsers() {
        return this.userService.getAllUsers();
    }

    @PostMapping
    public User addUser(@RequestBody @Valid User userToAdd) {
        return this.userService.createUser(userToAdd);
    }

    @GetMapping("{id}")
    public User getUserById(@PathVariable("id") int id) {
        return this.userService.getUserById(id)
                .orElse(null);
    }

    @PatchMapping
    public User updateUser(@RequestBody @Valid User userToUpdate) {
        System.out.println(userToUpdate.toString());
        return this.userService.updateUser(userToUpdate);
    }

    @DeleteMapping("{id}")
    public void deleteUserById(@PathVariable("id") int id) {
        this.userService.deleteUserById(id);
    }
}
