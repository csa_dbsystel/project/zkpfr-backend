package com.csa.backend.zkpfr.controller;

import com.csa.backend.zkpfr.model.Reply;
import com.csa.backend.zkpfr.services.IReplyService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.lang.NonNull;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import javax.validation.Valid;


@RestController
@RequestMapping(value = "/api/v1/replies")
public class ReplyController {

    private IReplyService replyService;

    @Autowired
    public ReplyController(IReplyService replyService) {
        this.replyService = replyService;
    }

    @GetMapping("/{id}")
    public Reply getReplyById(@PathVariable("id") int id) {
        return this.replyService.getReplyById(id)
                .orElse(null);
    }

    @PostMapping
    public Reply addReply(@RequestBody @Valid Reply replyToAdd) {
        return this.replyService.createOrUpdate(replyToAdd);
    }

    @PatchMapping
    public Reply updateReply(@RequestBody @Valid Reply replyToUpdate) {
        return replyService.createOrUpdate(replyToUpdate);
    }

    @GetMapping
    public Iterable<Reply> getAllReplies() {
        return this.replyService.getAllReplies();
    }

    @DeleteMapping("/{id}")
    public void deleteReplyById(@PathVariable("id") int id) {
        this.replyService.deleteReplyById(id);
    }

    @PostMapping("/up/{id}")
    public void upVoteReplyById(@PathVariable("id") int id) {
        this.replyService.upVoteReply(id);
    }

    @PostMapping("/down/{id}")
    public void downVoteReplyById(@PathVariable("id") int id) {
        this.replyService.downVoteReply(id);
    }

    @GetMapping("/posts/{id}")
    public Iterable<Reply> getRepliesByPostId(@PathVariable("id") int id) {
        return this.replyService.getRepliesByPostId(id);
    }


}
