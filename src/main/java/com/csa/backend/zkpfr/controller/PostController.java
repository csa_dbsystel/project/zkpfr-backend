package com.csa.backend.zkpfr.controller;

import com.csa.backend.zkpfr.model.Post;
import com.csa.backend.zkpfr.services.IPostService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.lang.NonNull;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping(value = "/api/v1/posts")

public class PostController {

    private final IPostService postService;

    @Autowired
    public PostController(IPostService postService) {
        this.postService = postService;
    }

    @PostMapping
    public Post addPost(@RequestBody @Valid Post postToAdd) {
        return this.postService.save(postToAdd);
    }

    @PatchMapping
    public Post updatePost(@RequestBody @Valid Post postToAdd) {
        return this.postService.save(postToAdd);
    }

    @GetMapping
    public Iterable<Post> getAllPosts() {
        return this.postService.findAll();
    }

    @GetMapping(path = "{id}")
    public Post getPostById(@PathVariable("id") int id) {
        return this.postService.findById(id)
            .orElse(null);
    }

    @DeleteMapping(path = "{id}")
    public void deletePostById(@PathVariable("id") int id) {
        this.postService.deleteById(id);
    }

    @GetMapping("latest")
    public Post getLatestFromAllPosts() {
        return this.postService.getLatestPost();
    }

    @GetMapping(path = "/channel/{id}")
    public Iterable<Post> getAllPostsByChannelId(@PathVariable("id") int id) {
        return this.postService.findAllByChannelId(id);
    }
}
