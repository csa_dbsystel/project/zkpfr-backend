package com.csa.backend.zkpfr.controller;

import com.csa.backend.zkpfr.model.User;
import com.csa.backend.zkpfr.model.UserCredential;
import com.csa.backend.zkpfr.services.IUserService;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(value = "/api/v1/auth")
public class AuthController {

    private final IUserService userService;

    public AuthController(IUserService userService) {
        this.userService = userService;
    }

    @GetMapping("/validateEmail/{email}")
    public boolean emailIsAvailable(@PathVariable String email) {
        return this.userService.emailIsAvailable(email);
    }

    @GetMapping("/validateUsername/{username}")
    public boolean usernameIsAvailable(@PathVariable String username) {
        return this.userService.usernameIsAvailable(username);
    }

    @PostMapping("/register")
    public User registerNewUser(@RequestBody UserCredential userCredential) {
        User createdUser = this.userService.registerNewUser(userCredential);
        if (createdUser != null) {
            return createdUser;
        } else {
            throw new IllegalStateException("Something went wrong!");
        }
    }

    @PostMapping("/login")
    public User authenticateUser(@RequestBody UserCredential userCredential) {
        final User requestedUserToLogin =
                this.userService.getUserIfCredentialsMatch(userCredential);
        if (requestedUserToLogin != null) {
            this.userService.updateUserLastAccess(requestedUserToLogin.getId());
            return requestedUserToLogin;
        } else {
            throw new IllegalStateException("Username or Password invalid!");
        }
    }
}
