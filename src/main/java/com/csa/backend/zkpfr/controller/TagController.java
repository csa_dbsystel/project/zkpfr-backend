package com.csa.backend.zkpfr.controller;

import com.csa.backend.zkpfr.model.Channel;
import com.csa.backend.zkpfr.model.Tag;
import com.csa.backend.zkpfr.services.IChannelService;
import com.csa.backend.zkpfr.services.ITagService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@RequestMapping(value = "/api/v1/tags")
public class TagController {

    private final ITagService tagService;

    @Autowired
    public TagController(ITagService tagService) {
        this.tagService = tagService;
    }

    @PostMapping
    public Tag addTag(@RequestBody @Valid Tag tagToAdd) {
        return this.tagService.save(tagToAdd);
    }

    @PatchMapping
    public Tag updateTag(@RequestBody @Valid Tag tagToAdd) {
        return this.tagService.save(tagToAdd);
    }

    @GetMapping
    public Iterable<Tag> getAllTags() {
        return this.tagService.findAll();
    }

    @GetMapping(path = "{id}")
    public Tag getTagById(@PathVariable("id") int id) {
        return this.tagService.findById(id)
            .orElse(null);
    }

    @GetMapping(path = "/post/{id}")
    public Iterable<Tag> getTagByPostId(@PathVariable("id") int id) {
        return this.tagService.findByPostId(id);
    }

    @DeleteMapping(path = "{id}")
    public void deletePostById(@PathVariable("id") int id) {
        this.tagService.deleteById(id);
    }
}
