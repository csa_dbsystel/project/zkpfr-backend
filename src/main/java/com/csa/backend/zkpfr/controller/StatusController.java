package com.csa.backend.zkpfr.controller;

import com.csa.backend.zkpfr.model.Channel;
import com.csa.backend.zkpfr.model.Status;
import com.csa.backend.zkpfr.services.IChannelService;
import com.csa.backend.zkpfr.services.IStatusService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@RequestMapping(value = "/api/v1/status")

public class StatusController {

    private final IStatusService statusService;

    @Autowired
    public StatusController(IStatusService statusService) {
        this.statusService = statusService;
    }

    @PostMapping
    public Status addStatus(@RequestBody @Valid Status statusToAdd) {
        return this.statusService.save(statusToAdd);
    }

    @PatchMapping
    public Status updateStatus(@RequestBody @Valid Status statusToAdd) {
        return this.statusService.save(statusToAdd);
    }

    @GetMapping
    public Iterable<Status> getAllStatus() {
        return this.statusService.findAll();
    }

    @GetMapping(path = "{id}")
    public Status getStatusById(@PathVariable("id") int id) {
        return this.statusService.findById(id)
            .orElse(null);
    }

    @DeleteMapping(path = "{id}")
    public void deleteStatusById(@PathVariable("id") int id) {
        this.statusService.deleteById(id);
    }
}
