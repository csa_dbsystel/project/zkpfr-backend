package com.csa.backend.zkpfr.services;

import com.csa.backend.zkpfr.model.User;
import com.csa.backend.zkpfr.model.UserCredential;

import java.util.List;
import java.util.Optional;

public interface IUserService {
    Optional<User> getUserById(int userId);

    boolean emailIsAvailable(String email);

    boolean usernameIsAvailable(String username);

    User registerNewUser(UserCredential userToCreate);

    User getUserIfCredentialsMatch(UserCredential userCredential);

    Iterable<User> getAllUsers();

    User createUser(User user);

    User updateUser(User user);

    void deleteUserById(int userId);

    void updateUserLastAccess(int userId);
}
