package com.csa.backend.zkpfr.services;

import com.csa.backend.zkpfr.model.Channel;
import com.csa.backend.zkpfr.model.Status;

import java.util.Optional;

public interface IStatusService {
    Status save(Status status);
    Optional<Status> findById(int id);
    Iterable<Status> findAll();
    void deleteById(int id);
}
