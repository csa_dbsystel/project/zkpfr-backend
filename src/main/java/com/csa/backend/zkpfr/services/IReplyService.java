package com.csa.backend.zkpfr.services;
import com.csa.backend.zkpfr.model.Reply;
import java.util.List;
import java.util.Optional;

public interface IReplyService {
    Optional<Reply> getReplyById(int id);

    Iterable<Reply> getAllReplies();

    Reply createOrUpdate(Reply reply);

    void deleteReplyById(int replyId);

    void upVoteReply(int replyId);

    void downVoteReply(int replyId);

    Iterable<Reply> getRepliesByPostId(int postId);

}
