package com.csa.backend.zkpfr.services;

import com.csa.backend.zkpfr.config.PasswordEncryption;
import com.csa.backend.zkpfr.model.User;
import com.csa.backend.zkpfr.model.UserCredential;
import com.csa.backend.zkpfr.repositories.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class UserService implements IUserService {

    private UserRepository userRepository;

    public UserService(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @Override
    public Optional<User> getUserById(int userId) {
        return userRepository.findById(userId);
    }

    @Override
    public boolean emailIsAvailable(String email) {
        return this.userRepository.findByEmail(email).isEmpty();
    }

    @Override
    public boolean usernameIsAvailable(String username) {
        return this.userRepository.findByDisplayName(username).isEmpty();
    }

    @Override
    public User registerNewUser(UserCredential userToCreate) {
        if (!this.usernameIsAvailable(userToCreate.getDisplayName()) || !this.emailIsAvailable(userToCreate.getEmail())) {
            return null;
        } else {
            PasswordEncryption encryption = new PasswordEncryption(userToCreate.getPassword());
            final String encodedPassword = encryption.encryptUserPassword();
            User newUser = new User(
                    userToCreate.getDisplayName(),
                    userToCreate.getEmail(),
                    encodedPassword,
                    ""
            );
            final Timestamp currentTime = new Timestamp(System.currentTimeMillis());
            newUser.setCreationDate(currentTime);
            newUser.setLastAccessDate(currentTime);
            this.userRepository.save(newUser);
            return this.userRepository.findByDisplayName(newUser.getDisplayName()).get(0);
        }
    }

    @Override
    public User getUserIfCredentialsMatch(UserCredential userCredential) {
        try {
            final User userToLogin = this.userRepository.findByEmail(userCredential.getEmail()).get(0);
            PasswordEncryption encryption = new PasswordEncryption(userCredential.getPassword());
            final String encodedPassword = encryption.encryptUserPassword();
            return userToLogin.getPassHash().equals(encodedPassword) ? userToLogin : null;
        } catch (IndexOutOfBoundsException e) {
            return null;
        }
    }

    @Override
    public Iterable<User> getAllUsers() {
        List<User> users = new ArrayList<>();
        userRepository.findAll().forEach(e -> users.add(e));
        return users;
    }

    @Override
    public User updateUser(User user) {
        if(user.getPassHash() == null){
            user.setPassHash(this.getUserById(user.getId()).get().getPassHash());
        }
        return userRepository.save(user);
    }

    @Override
    public User createUser(User user) {
        return userRepository.save(user);
    }

    @Override
    public void deleteUserById(int userId) {
        userRepository.deleteById(userId);
    }

    @Override
    public void updateUserLastAccess(int userId) {
        User user = this.getUserById(userId).get();
        if (user != null) {
            user.setLastAccessDate(new Timestamp(System.currentTimeMillis()));
            this.updateUser(user);
        }
    }
}
