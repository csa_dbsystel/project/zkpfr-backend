package com.csa.backend.zkpfr.services;

import com.csa.backend.zkpfr.model.Channel;
import com.csa.backend.zkpfr.model.Tag;
import com.csa.backend.zkpfr.repositories.ChannelRepository;
import com.csa.backend.zkpfr.repositories.PostRepository;
import com.csa.backend.zkpfr.repositories.TagRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class TagService implements ITagService {

    private TagRepository tagRepository;
    private IPostService postService;

    public TagService(TagRepository tagRepository, IPostService postService) {
        this.tagRepository = tagRepository;
        this.postService = postService;
    }

    @Override
    public Tag save(Tag tag) {
        return tagRepository.save(tag);
    }

    @Override
    public Optional<Tag> findById(int id) {
        return tagRepository.findById(id);
    }

    @Override
    public Iterable<Tag> findByPostId(int id) {
        return tagRepository.findByPost(this.postService.findById(id).orElse(null));
    }

    @Override
    public Iterable<Tag> findAll() {
        List<Tag> tags = new ArrayList<>();
        tagRepository.findAll()
                .forEach(result -> tags.add(result));
        return tags;
    }

    @Override
    public void deleteById(int id) {
        tagRepository.deleteById(id);
    }
}
