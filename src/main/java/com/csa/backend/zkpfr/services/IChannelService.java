package com.csa.backend.zkpfr.services;

import com.csa.backend.zkpfr.model.Channel;
import com.csa.backend.zkpfr.model.Post;

import java.util.Optional;

public interface IChannelService {
    Channel save(Channel channel);
    Optional<Channel> findById(int id);
    Iterable<Channel> findAll();
    void deleteById(int id);
}
