package com.csa.backend.zkpfr.services;
import com.csa.backend.zkpfr.model.Reply;
import com.csa.backend.zkpfr.repositories.ReplyRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class ReplyService implements IReplyService {
    @Autowired
    private ReplyRepository replyRepository;
    @Autowired
    private PostService postService;

    @Override
    public Optional<Reply> getReplyById(int id) {
        return replyRepository.findById(id);
    }

    @Override
    public Reply createOrUpdate(Reply reply) {
        return replyRepository.save(reply);
    }

    @Override
    public void deleteReplyById(int replyId) {
        replyRepository.deleteById((replyId));
    }

    @Override
    public Iterable<Reply> getAllReplies() {
        List<Reply> replies = new ArrayList<>();
        replyRepository.findAll().forEach(e -> replies.add(e));
        return replies;
    }

    @Override
    public void upVoteReply(int replyId) {
        Reply reply = this.getReplyById(replyId).orElse(null);
        if(reply != null) {
            reply.setLikes(reply.getLikes() + 1);
            createOrUpdate(reply);
        }
    }

    @Override
    public void downVoteReply(int replyId) {
        Reply reply = this.getReplyById(replyId).orElse(null);
        if(reply != null) {
            reply.setLikes(reply.getLikes() - 1);
            createOrUpdate(reply);
        }
    }

    @Override
    public Iterable<Reply> getRepliesByPostId(int postId) {
        List<Reply> replies = new ArrayList<>();
        replies = replyRepository.findAllByPost(this.postService.findById(postId).get());
        return replies;
    }

}


