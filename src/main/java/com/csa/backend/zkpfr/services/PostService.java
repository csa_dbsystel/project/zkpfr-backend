package com.csa.backend.zkpfr.services;

import com.csa.backend.zkpfr.model.Post;
import com.csa.backend.zkpfr.repositories.PostRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class PostService implements IPostService {

    private PostRepository postRepository;
    private ChannelService channelService;

    public PostService(PostRepository postRepository, ChannelService channelService) {
        this.postRepository = postRepository;
        this.channelService = channelService;
    }

    @Override
    public Post save(Post post) {
        try {
            return postRepository.save(post);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public Optional<Post> findById(int id) {
        return postRepository.findById(id);
    }

    @Override
    public Iterable<Post> findAll() {
        List<Post> posts = new ArrayList<>();
        postRepository.findAll()
                .forEach(result -> posts.add(result));
        return posts;
    }

    @Override
    public void deleteById(int id) {
        postRepository.deleteById(id);
    }

    public Post getLatestPost() {
        return postRepository.findAllByOrderByCreationDateDesc().get(0);
    }

    @Override
    public Iterable<Post> findAllByChannelId(int id) {
        List<Post> posts = new ArrayList<>();
        posts = postRepository.findAllByChannel(this.channelService.findById(id).get());
        return posts;
    }
}
