package com.csa.backend.zkpfr.services;

import com.csa.backend.zkpfr.model.Channel;
import com.csa.backend.zkpfr.model.Tag;

import java.util.Optional;

public interface ITagService {
    Tag save(Tag tag);
    Optional<Tag> findById(int id);
    Iterable<Tag> findByPostId(int id);
    Iterable<Tag> findAll();
    void deleteById(int id);
}
