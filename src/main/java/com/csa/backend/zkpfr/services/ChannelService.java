package com.csa.backend.zkpfr.services;

import com.csa.backend.zkpfr.model.Channel;
import com.csa.backend.zkpfr.model.Post;
import com.csa.backend.zkpfr.repositories.ChannelRepository;
import com.csa.backend.zkpfr.repositories.PostRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class ChannelService implements IChannelService {
    @Autowired
    private ChannelRepository channelRepository;

    @Override
    public Channel save(Channel channel) {
        return channelRepository.save(channel);
    }

    @Override
    public Optional<Channel> findById(int id) {
        return channelRepository.findById(id);
    }

    @Override
    public Iterable<Channel> findAll() {
        List<Channel> channels = new ArrayList<>();
        channelRepository.findAll()
                .forEach(result -> channels.add(result));
        return channels;
    }

    @Override
    public void deleteById(int id) {
        channelRepository.deleteById(id);
    }
}
