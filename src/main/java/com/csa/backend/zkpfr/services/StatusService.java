package com.csa.backend.zkpfr.services;

import com.csa.backend.zkpfr.model.Channel;
import com.csa.backend.zkpfr.model.Status;
import com.csa.backend.zkpfr.repositories.ChannelRepository;
import com.csa.backend.zkpfr.repositories.StatusRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class StatusService implements IStatusService {

    private StatusRepository statusRepository;

    public StatusService(StatusRepository statusRepository) {
        this.statusRepository = statusRepository;
    }

    @Override
    public Status save(Status status) {
        return statusRepository.save(status);
    }

    @Override
    public Optional<Status> findById(int id) {
        return statusRepository.findById(id);
    }

    @Override
    public Iterable<Status> findAll() {
        List<Status> statuses = new ArrayList<>();
        statusRepository.findAll()
                .forEach(result -> statuses.add(result));
        return statuses;
    }

    @Override
    public void deleteById(int id) {
        statusRepository.deleteById(id);
    }
}
