package com.csa.backend.zkpfr.services;

import com.csa.backend.zkpfr.model.Post;

import java.util.Optional;

public interface IPostService {
    Post save(Post post);
    Optional<Post> findById(int id);
    Iterable<Post> findAll();
    void deleteById(int id);
    Post getLatestPost();
    Iterable<Post> findAllByChannelId(int id);
}
