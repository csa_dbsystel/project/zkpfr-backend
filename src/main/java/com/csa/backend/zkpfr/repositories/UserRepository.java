package com.csa.backend.zkpfr.repositories;

import com.csa.backend.zkpfr.model.User;

import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface UserRepository extends CrudRepository<User, Integer> {
    List<User> findByDisplayName(String displayName);

    List<User> findByEmail(String email) throws IndexOutOfBoundsException;
}

