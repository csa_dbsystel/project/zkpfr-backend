package com.csa.backend.zkpfr.repositories;

import com.csa.backend.zkpfr.model.Post;
import com.csa.backend.zkpfr.model.Reply;

import org.springframework.data.repository.CrudRepository;


import java.util.List;

public interface ReplyRepository extends CrudRepository<Reply, Integer> {
    List<Reply> findAllByPost(Post post);
}
