package com.csa.backend.zkpfr.repositories;

import com.csa.backend.zkpfr.model.Channel;
import com.csa.backend.zkpfr.model.Post;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface ChannelRepository extends CrudRepository<Channel, Integer> {
}
