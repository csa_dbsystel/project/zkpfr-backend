package com.csa.backend.zkpfr.repositories;

import com.csa.backend.zkpfr.model.Post;
import com.csa.backend.zkpfr.model.Tag;
import org.springframework.data.repository.CrudRepository;

import java.util.List;


public interface TagRepository extends CrudRepository<Tag, Integer> {
    List<Tag> findByPost(Post post);
}
