package com.csa.backend.zkpfr.repositories;

import com.csa.backend.zkpfr.model.Status;
import org.springframework.data.repository.CrudRepository;

public interface StatusRepository extends CrudRepository<Status, Integer> {
}