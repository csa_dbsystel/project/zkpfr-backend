package com.csa.backend.zkpfr.repositories;

import com.csa.backend.zkpfr.model.Channel;
import com.csa.backend.zkpfr.model.Post;
import com.csa.backend.zkpfr.model.User;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface PostRepository extends CrudRepository<Post, Integer> {
    List<Post> findAllByOrderByCreationDateDesc();
    List<Post> findAllByChannel(Channel channel);

}
