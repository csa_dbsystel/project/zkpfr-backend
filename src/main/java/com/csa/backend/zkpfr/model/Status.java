package com.csa.backend.zkpfr.model;

import javax.persistence.*;
import java.io.Serializable;;

/**
 * a status of a post
 */
@Entity
@Table(name="T_STATUS")
public class Status implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy= GenerationType.AUTO)
    @Column(name="ID")
    private int id;
    @Column(name="Name")
    private String name;
    @Column(name="Short")
    private String short_;


    public Status() {
    }

    public Status(String name, String short_) {
        this.name = name;
        this.short_ = short_;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getShort() {
        return short_;
    }

    public void setShort(String short_) {
        this.short_ = short_;
    }

    @Override
    public String toString() {
        return "Status{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", short_='" + short_ + '\'' +
                '}';
    }
}
