package com.csa.backend.zkpfr.model;

import javax.persistence.*;
import java.io.Serializable;
import java.sql.Timestamp;
import java.util.List;

@Entity
@Table(name="T_POSTS")
public class Post implements Serializable{
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy= GenerationType.AUTO)
    @Column(name="ID")
    private int id;
    @Column(name="CreationDate")
    private Timestamp creationDate;
    @Column(name="ViewCount")
    private int viewCount;
    @ManyToOne
    @JoinColumn(name = "OwnerUserID", referencedColumnName = "ID")
    private User user;
    @Column(name="Title")
    private String title;
    @Column(name="Body")
    private String body;
    @ManyToOne
    @JoinColumn(name = "ChannelID", referencedColumnName = "ID")
    private Channel channel;
    @ManyToOne
    @JoinColumn(name = "StatusID", referencedColumnName = "ID")
    private Status status;

    public Post() {

    }

    public Post(Timestamp creationDate, User user, String title, String body, Channel channel, Status status) {
        this.creationDate = creationDate;
        this.user = user;
        this.title = title;
        this.body = body;
        this.channel = channel;
        this.status = status;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Timestamp getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(Timestamp creationDate) {
        this.creationDate = creationDate;
    }

    public int getViewCount() {
        return viewCount;
    }

    public void setViewCount(int viewCount) {
        this.viewCount = viewCount;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getBody() {
        return body;
    }

    public void setBody(String body) {
        this.body = body;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Channel getChannel() {
        return channel;
    }

    public void setChannel(Channel channel) {
        this.channel = channel;
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }


    @Override
    public String toString() {
        return "Post{" +
                "id=" + id +
                ", creationDate=" + creationDate +
                ", viewCount=" + viewCount +
                ", user=" + user.toString() +
                ", title='" + title + '\'' +
                ", body='" + body + '\'' +
                ", channel=" + channel.toString() +
                ", status=" + status.toString() +
                '}';
    }
}
