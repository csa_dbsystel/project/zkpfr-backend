package com.csa.backend.zkpfr.model;

import java.io.Serializable;

public class UserCredential implements Serializable {
    private String email;
    private String password;
    private String displayName;

    UserCredential() {
    }

    UserCredential(String email, String password) {
        this.email = email;
        this.password = password;
        this.displayName = "";
    }

    public UserCredential(String email, String password, String displayName) {
        this.email = email;
        this.password = password;
        this.displayName = displayName;
    }

    public String getDisplayName() {
        return displayName;
    }

    public void setDisplayName(String displayName) {
        this.displayName = displayName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
