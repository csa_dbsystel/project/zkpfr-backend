package com.csa.backend.zkpfr.model;

import javax.persistence.*;
import java.io.Serializable;

/**
 * a tag to describe the content of a post
 */
@Entity
@Table(name="T_TAGS")
public class Tag implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy= GenerationType.AUTO)
    @Column(name="ID")
    int id;
    @Column(name="TagName")
    private String tagName;
    @OneToOne
    @JoinColumn(name = "PostID", referencedColumnName = "ID")
    private Post post;


    public Tag() {
    }

    public Tag(int id, String tagName, Post post) {
        this.id = id;
        this.tagName = tagName;
        this.post = post;
    }

    public Tag(int id, String tagName) {
        this.id = id;
        this.tagName = tagName;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTagName() {
        return tagName;
    }

    public void setTagName(String tagName) {
        this.tagName = tagName;
    }

    public Post getPost() {
        return post;
    }

    public void setPost(Post post) {
        this.post = post;
    }

    @Override
    public String toString() {
        return "Tag{" +
                "id=" + id +
                ", tagName='" + tagName + '\'' +
                ", post=" + post +
                '}';
    }
}
