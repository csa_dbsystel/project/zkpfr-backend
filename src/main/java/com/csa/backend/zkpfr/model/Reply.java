package com.csa.backend.zkpfr.model;

import javax.persistence.*;
import java.io.Serializable;
import java.sql.Timestamp;

/**
 * a reply to a Post
 */
@Entity
@Table(name="T_REPLIES")
public class Reply implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy= GenerationType.AUTO)
    @Column(name="ID")
    private int id;
    @OneToOne
    @JoinColumn(name = "PostID", referencedColumnName = "ID")
    private Post post;
    @OneToOne
    @JoinColumn(name = "ReplierID", referencedColumnName = "ID")
    private User replier;
    @Column(name="Text")
    private String text;
    @Column(name="Likes")
    private int likes;
    @Column(name="CreationDate")
    private Timestamp creationDate;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Post getPost() {
        return post;
    }

    public void setPost(Post post) {
        this.post = post;
    }

    public User getReplier() {
        return replier;
    }

    public void setReplier(User replier) {
        this.replier = replier;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public int getLikes() {
        return likes;
    }

    public void setLikes(int likes) {
        this.likes = likes;
    }

    public Timestamp getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(Timestamp creationDate) {
        this.creationDate = creationDate;
    }

    @Override
    public String toString() {
        return "Reply{" +
                "id=" + id +
                ", post=" + post +
                ", replier=" + replier +
                ", text='" + text + '\'' +
                ", likes=" + likes +
                ", creationDate=" + creationDate +
                '}';
    }
}
