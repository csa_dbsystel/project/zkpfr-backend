package com.csa.backend.zkpfr.model;

import javax.persistence.*;
import java.io.Serializable;

/**
 * a channel to order the posts to specific topics
 */
@Entity
@Table(name="T_CHANNELS")
public class Channel implements Serializable{
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy= GenerationType.AUTO)
    @Column(name="ID")
    private int id;
    @Column(name="Name")
    private String name;

    public Channel(String name) {
        this.name = name;
    }

    public Channel() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return "Channel{" +
                "id=" + id +
                ", name='" + name + '\'' +
                '}';
    }
}
