package com.csa.backend.zkpfr.model;

import com.fasterxml.jackson.annotation.JsonProperty;

import javax.persistence.*;
import java.io.Serializable;
import java.sql.Timestamp;

/**
 * a user in the system
 */
@Entity
@Table(name = "T_USERS")
public class User implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "ID")
    private int id;
    @Column(name = "CreationDate")
    private Timestamp creationDate;
    @Column(name = "LastAccesDate")
    private Timestamp lastAccessDate;
    @Column(name = "DisplayName")
    private String displayName;
    @Column(name = "Reputation")
    private int reputation;
    @Column(name = "Email")
    private String email;
    @Column(name = "PassHash")
    private String passHash;
    @Column(name = "AboutMe")
    private String aboutMe;
    @Column(name = "Activated")
    private int activated;
    @Column(name = "AvatarId")
    private int avatarId;

    User() {

    }

    public User(String displayName, String email, String passHash, String aboutMe) {
        this.displayName = displayName;
        this.email = email;
        this.passHash = passHash;
        this.aboutMe = aboutMe;
    }


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getDisplayName() {
        return displayName;
    }

    public void setDisplayName(String displayName) {
        this.displayName = displayName;
    }

    public int getReputation() {
        return reputation;
    }

    public void setReputationID(int reputation) {
        this.reputation = reputation;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    @JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
    public String getPassHash() {
        return passHash;
    }

    public void setPassHash(String passHash) {
        this.passHash = passHash;
    }

    public String getAboutMe() {
        return aboutMe;
    }

    public void setAboutMe(String aboutMe) {
        this.aboutMe = aboutMe;
    }

    public int isActivated() {
        return activated;
    }

    public void setActivated(int activated) {
        this.activated = activated;
    }

    public Timestamp getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(Timestamp creationDate) {
        this.creationDate = creationDate;
    }

    public Timestamp getLastAccessDate() {
        return lastAccessDate;
    }

    public void setLastAccessDate(Timestamp lastAccessDate) {
        this.lastAccessDate = lastAccessDate;
    }

    public int getActivated() {
        return activated;
    }

    public int getAvatarId() {
        return avatarId;
    }

    public void setAvatarId(int avatarId) {
        this.avatarId = avatarId;
    }

    public void setReputation(int reputation) {
        this.reputation = reputation;
    }

    @Override
    public String toString() {
        return "User{" +
                "id=" + id +
                ", creationDate=" + creationDate +
                ", lastAccessDate=" + lastAccessDate +
                ", displayName='" + displayName + '\'' +
                ", reputation=" + reputation +
                ", email='" + email + '\'' +
                ", passHash='" + passHash + '\'' +
                ", aboutMe='" + aboutMe + '\'' +
                ", activated=" + activated +
                ", avatarId=" + avatarId +
                '}';
    }
}
