package com.csa.backend.zkpfr.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.web.servlet.config.annotation.CorsRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

@Configuration
public class SpringSecurityConfig extends WebSecurityConfigurerAdapter implements WebMvcConfigurer {

    // Create 2 users for demo
    @Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception {

        auth.inMemoryAuthentication()
                .withUser("anonym").password("{noop}w(S9<tTmC(bkPpP4)HLR}]2Y8D-fn;").roles("ANONYM")
                .and()
                .withUser("user").password("{noop}w(S9<tTmC(bkPpP4)HLR}]2Y8D-fn;").roles("ANONYM", "USER")
                .and()
                .withUser("admin").password("{noop}w(S9<tTmC(bkPpP4)HLR}]2Y8D-fn;").roles("ANONYM", "USER", "ADMIN");

    }

    // Secure the endpoins with HTTP Basic authentication
    @Override
    protected void configure(HttpSecurity http) throws Exception {

        http
                //HTTP Basic authentication
                .httpBasic()
                .and()
                .authorizeRequests()
                .antMatchers(HttpMethod.GET, "/api/v1/**").hasRole("ANONYM")
                .antMatchers(HttpMethod.POST, "/api/v1/**").hasRole("USER")
                .antMatchers(HttpMethod.PUT, "/api/v1/**").hasRole("USER")
                .antMatchers(HttpMethod.PATCH, "/api/v1/**").hasRole("USER")
                .antMatchers(HttpMethod.DELETE, "/api/v1/**").hasRole("ADMIN")
                .and()
                .csrf().disable()
                .formLogin().disable()

        ;
    }

    @Override
    public void addCorsMappings(CorsRegistry registry) {
        registry.addMapping("/**")
                .allowedOrigins("http://5.135.157.83", "http://localhost:4200")
                .allowedMethods("GET", "POST", "PUT", "PATCH", "DELETE")
        ;
    }
    // , "http://localhost:4200", "http://127.0.0.1:5500"

}
