package com.csa.backend.zkpfr.config;

import org.apache.commons.codec.binary.Hex;

import javax.crypto.SecretKey;
import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.PBEKeySpec;
import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;

public class PasswordEncryption {
    private String password;

    public PasswordEncryption(String password) {
        this.password = password;
    }

    public String encryptUserPassword() {
        final String SALT = "2svJA*GMFvTg";
        char[] passwordChars = password.toCharArray();
        byte[] saltBytes = SALT.getBytes();
        byte[] hashedPW = hashPassword(passwordChars, saltBytes);
        return Hex.encodeHexString(hashedPW);
    }

    private static byte[] hashPassword(char[] passwordChars, byte[] saltBytes) {
        final int ITERATIONS = 10000;
        final int KEY_LENGTH = 512;
        try {
            SecretKeyFactory skf = SecretKeyFactory.getInstance("PBKDF2WithHmacSHA512");
            PBEKeySpec spec = new PBEKeySpec(passwordChars, saltBytes, ITERATIONS, KEY_LENGTH);
            SecretKey key = skf.generateSecret(spec);
            return key.getEncoded();
        } catch (NoSuchAlgorithmException | InvalidKeySpecException e) {
            throw new RuntimeException(e);
        }
    }
}
