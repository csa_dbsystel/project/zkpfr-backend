package com.csa.backend.zkpfr.services;

import com.csa.backend.zkpfr.model.*;
import com.csa.backend.zkpfr.repositories.TagRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;

import java.sql.Timestamp;
import java.util.Arrays;
import java.util.Optional;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.mockito.MockitoAnnotations.initMocks;

class TagServiceTest {

    @Mock
    private TagRepository mockTagRepository;
    @Mock
    private IPostService mockPostService;

    @InjectMocks
    private TagService tagServiceUnderTest;

    @BeforeEach
    void setUp() {
        initMocks(this);
    }

    @Test
    void testSave() {
        // Setup
        final Tag tag = new Tag(0, "tagName");
        when(mockTagRepository.save(any(Tag.class))).thenReturn(new Tag(0, "tagName"));

        // Run the test
        final Tag result = tagServiceUnderTest.save(tag);

        // Verify the results
    }

    @Test
    void testFindById() {
        // Setup
        when(mockTagRepository.findById(0)).thenReturn(Optional.of(new Tag(0, "tagName")));

        // Run the test
        final Optional<Tag> result = tagServiceUnderTest.findById(0);

        // Verify the results
    }

    @Test
    void testFindByPostId() {
        // Setup
        when(mockTagRepository.findByPost(any(Post.class))).thenReturn(Arrays.asList(new Tag(0, "tagName")));

        // Configure IPostService.findById(...).
        final Optional<Post> post = Optional.of(new Post(new Timestamp(0L), new User("displayName", "email", "passHash", "aboutMe"), "title", "body", new Channel("name"), new Status("name", "short_")));
        when(mockPostService.findById(0)).thenReturn(post);

        // Run the test
        final Iterable<Tag> result = tagServiceUnderTest.findByPostId(0);

        // Verify the results
    }

    @Test
    void testFindAll() {
        // Setup
        when(mockTagRepository.findAll()).thenReturn(Arrays.asList(new Tag(0, "tagName")));

        // Run the test
        final Iterable<Tag> result = tagServiceUnderTest.findAll();

        // Verify the results
    }

    @Test
    void testDeleteById() {
        // Setup

        // Run the test
        tagServiceUnderTest.deleteById(0);

        // Verify the results
        verify(mockTagRepository).deleteById(0);
    }
}
