package com.csa.backend.zkpfr.services;

import com.csa.backend.zkpfr.model.Channel;
import com.csa.backend.zkpfr.model.Post;
import com.csa.backend.zkpfr.model.Status;
import com.csa.backend.zkpfr.model.User;
import com.csa.backend.zkpfr.repositories.PostRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;

import java.sql.Timestamp;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.mockito.MockitoAnnotations.initMocks;

class PostServiceTest {

    @Mock
    private PostRepository mockPostRepository;
    @Mock
    private ChannelService mockChannelService;

    private PostService postServiceUnderTest;

    @BeforeEach
    void setUp() {
        initMocks(this);
        postServiceUnderTest = new PostService(mockPostRepository, mockChannelService);
    }

    @Test
    void testSave() {
        // Setup
        final Post post = new Post(new Timestamp(0L), new User("displayName", "email", "passHash", "aboutMe"), "title", "body", new Channel("name"), new Status("name", "short_"));

        // Configure PostRepository.save(...).
        final Post post1 = new Post(new Timestamp(0L), new User("displayName", "email", "passHash", "aboutMe"), "title", "body", new Channel("name"), new Status("name", "short_"));
        when(mockPostRepository.save(any(Post.class))).thenReturn(post1);

        // Run the test
        final Post result = postServiceUnderTest.save(post);

        // Verify the results
    }

    @Test
    void testFindById() {
        // Setup

        // Configure PostRepository.findById(...).
        final Optional<Post> post = Optional.of(new Post(new Timestamp(0L), new User("displayName", "email", "passHash", "aboutMe"), "title", "body", new Channel("name"), new Status("name", "short_")));
        when(mockPostRepository.findById(0)).thenReturn(post);

        // Run the test
        final Optional<Post> result = postServiceUnderTest.findById(0);

        // Verify the results
    }

    @Test
    void testFindAll() {
        // Setup

        // Configure PostRepository.findAll(...).
        final Iterable<Post> posts = Arrays.asList(new Post(new Timestamp(0L), new User("displayName", "email", "passHash", "aboutMe"), "title", "body", new Channel("name"), new Status("name", "short_")));
        when(mockPostRepository.findAll()).thenReturn(posts);

        // Run the test
        final Iterable<Post> result = postServiceUnderTest.findAll();

        // Verify the results
    }

    @Test
    void testDeleteById() {
        // Setup

        // Run the test
        postServiceUnderTest.deleteById(0);

        // Verify the results
        verify(mockPostRepository).deleteById(0);
    }

    @Test
    void testGetLatestPost() {
        // Setup

        // Configure PostRepository.findAllByOrderByCreationDateDesc(...).
        final List<Post> posts = Arrays.asList(new Post(new Timestamp(0L), new User("displayName", "email", "passHash", "aboutMe"), "title", "body", new Channel("name"), new Status("name", "short_")));
        when(mockPostRepository.findAllByOrderByCreationDateDesc()).thenReturn(posts);

        // Run the test
        final Post result = postServiceUnderTest.getLatestPost();

        // Verify the results
    }

    @Test
    void testFindAllByChannelId() {
        // Setup

        // Configure PostRepository.findAllByChannel(...).
        final List<Post> posts = Arrays.asList(new Post(new Timestamp(0L), new User("displayName", "email", "passHash", "aboutMe"), "title", "body", new Channel("name"), new Status("name", "short_")));
        when(mockPostRepository.findAllByChannel(any(Channel.class))).thenReturn(posts);

        when(mockChannelService.findById(0)).thenReturn(Optional.of(new Channel("name")));

        // Run the test
        final Iterable<Post> result = postServiceUnderTest.findAllByChannelId(0);

        // Verify the results
    }
}
