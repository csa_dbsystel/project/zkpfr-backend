package com.csa.backend.zkpfr.services;

import com.csa.backend.zkpfr.model.User;
import com.csa.backend.zkpfr.model.UserCredential;
import com.csa.backend.zkpfr.repositories.UserRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.mockito.MockitoAnnotations.initMocks;

class UserServiceTest {

    @Mock
    private UserRepository mockUserRepository;

    @InjectMocks
    private UserService userServiceUnderTest;

    @BeforeEach
    void setUp() {
        initMocks(this);
    }

    @Test
    void testGetUserById() {
        // Setup

        // Configure UserRepository.findById(...).
        final Optional<User> user = Optional.of(new User("displayName", "email", "passHash", "aboutMe"));
        when(mockUserRepository.findById(0)).thenReturn(user);

        // Run the test
        final Optional<User> result = userServiceUnderTest.getUserById(0);

        // Verify the results
    }

    @Test
    void testEmailIsAvailable() {
        // Setup

        // Configure UserRepository.findByEmail(...).
        final List<User> users = Arrays.asList(new User("displayName", "email", "passHash", "aboutMe"));
        when(mockUserRepository.findByEmail("email")).thenReturn(users);

        // Run the test
        final boolean result = userServiceUnderTest.emailIsAvailable("email");

        // Verify the results
        assertFalse(result);
    }


    @Test
    void testUsernameIsAvailable() {
        // Setup

        // Configure UserRepository.findByDisplayName(...).
        final List<User> users = Arrays.asList(new User("displayName", "email", "passHash", "aboutMe"));
        when(mockUserRepository.findByDisplayName("displayName")).thenReturn(users);

        // Run the test
        final boolean result = userServiceUnderTest.usernameIsAvailable("username");

        // Verify the results
        assertTrue(result);
    }

    @Test
    void testRegisterNewUser() {
        // Setup
        final UserCredential userToCreate = new UserCredential("email", "password", "displayName");

        // Configure UserRepository.findByDisplayName(...).
        final List<User> users = Arrays.asList(new User("displayName", "email", "passHash", "aboutMe"));
        when(mockUserRepository.findByDisplayName("displayName")).thenReturn(users);

        // Configure UserRepository.findByEmail(...).
        final List<User> users1 = Arrays.asList(new User("displayName", "email", "passHash", "aboutMe"));
        when(mockUserRepository.findByEmail("email")).thenReturn(users1);

        // Configure UserRepository.save(...).
        final User user = new User("displayName", "email", "passHash", "aboutMe");
        when(mockUserRepository.save(any(User.class))).thenReturn(user);

        // Run the test
        final User result = userServiceUnderTest.registerNewUser(userToCreate);

        // Verify the results
    }

    @Test
    void testRegisterNewUser_UserRepositoryThrowsIndexOutOfBoundsException() {
        // Setup
        final UserCredential userToCreate = new UserCredential("email", "password", "displayName");

        // Configure UserRepository.findByDisplayName(...).
        final List<User> users = Arrays.asList(new User("displayName", "email", "passHash", "aboutMe"));
        when(mockUserRepository.findByDisplayName("displayName")).thenReturn(users);

        when(mockUserRepository.findByEmail("email")).thenThrow(IndexOutOfBoundsException.class);

        // Run the test
        final User result = userServiceUnderTest.registerNewUser(userToCreate);

        // Verify the results
    }

    @Test
    void testGetUserIfCredentialsMatch() {
        // Setup
        final UserCredential userCredential = new UserCredential("email", "password", "displayName");

        // Configure UserRepository.findByEmail(...).
        final List<User> users = Arrays.asList(new User("displayName", "email", "passHash", "aboutMe"));
        when(mockUserRepository.findByEmail("email")).thenReturn(users);

        // Run the test
        final User result = userServiceUnderTest.getUserIfCredentialsMatch(userCredential);

        // Verify the results
    }

    @Test
    void testGetUserIfCredentialsMatch_UserRepositoryThrowsIndexOutOfBoundsException() {
        // Setup
        final UserCredential userCredential = new UserCredential("email", "password", "displayName");
        when(mockUserRepository.findByEmail("email")).thenThrow(IndexOutOfBoundsException.class);

        // Run the test
        final User result = userServiceUnderTest.getUserIfCredentialsMatch(userCredential);

        // Verify the results
    }

    @Test
    void testGetAllUsers() {
        // Setup

        // Configure UserRepository.findAll(...).
        final Iterable<User> users = Arrays.asList(new User("displayName", "email", "passHash", "aboutMe"));
        when(mockUserRepository.findAll()).thenReturn(users);

        // Run the test
        final Iterable<User> result = userServiceUnderTest.getAllUsers();

        // Verify the results
    }

    @Test
    void testUpdateUser() {
        // Setup
        final User user = new User("displayName", "email", "passHash", "aboutMe");

        // Configure UserRepository.findById(...).
        final Optional<User> user1 = Optional.of(new User("displayName", "email", "passHash", "aboutMe"));
        when(mockUserRepository.findById(0)).thenReturn(user1);

        // Configure UserRepository.save(...).
        final User user2 = new User("displayName", "email", "passHash", "aboutMe");
        when(mockUserRepository.save(any(User.class))).thenReturn(user2);

        // Run the test
        final User result = userServiceUnderTest.updateUser(user);

        // Verify the results
    }

    @Test
    void testCreateUser() {
        // Setup
        final User user = new User("displayName", "email", "passHash", "aboutMe");

        // Configure UserRepository.save(...).
        final User user1 = new User("displayName", "email", "passHash", "aboutMe");
        when(mockUserRepository.save(any(User.class))).thenReturn(user1);

        // Run the test
        final User result = userServiceUnderTest.createUser(user);

        // Verify the results
    }

    @Test
    void testDeleteUserById() {
        // Setup

        // Run the test
        userServiceUnderTest.deleteUserById(0);

        // Verify the results
        verify(mockUserRepository).deleteById(0);
    }

    @Test
    void testUpdateUserLastAccess() {
        // Setup

        // Configure UserRepository.findById(...).
        final Optional<User> user = Optional.of(new User("displayName", "email", "passHash", "aboutMe"));
        when(mockUserRepository.findById(0)).thenReturn(user);

        // Configure UserRepository.save(...).
        final User user1 = new User("displayName", "email", "passHash", "aboutMe");
        when(mockUserRepository.save(any(User.class))).thenReturn(user1);

        // Run the test
        userServiceUnderTest.updateUserLastAccess(0);

        // Verify the results
    }
}
