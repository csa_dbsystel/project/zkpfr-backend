package com.csa.backend.zkpfr.services;

import com.csa.backend.zkpfr.model.Status;
import com.csa.backend.zkpfr.repositories.StatusRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;

import java.util.Arrays;
import java.util.Optional;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.mockito.MockitoAnnotations.initMocks;

class StatusServiceTest {

    @Mock
    private StatusRepository mockStatusRepository;

    @InjectMocks
    private StatusService statusServiceUnderTest;

    @BeforeEach
    void setUp() {
        initMocks(this);
    }

    @Test
    void testSave() {
        // Setup
        final Status status = new Status("name", "short_");
        when(mockStatusRepository.save(any(Status.class))).thenReturn(new Status("name", "short_"));

        // Run the test
        final Status result = statusServiceUnderTest.save(status);

        // Verify the results
    }

    @Test
    void testFindById() {
        // Setup

        // Configure StatusRepository.findById(...).
        final Optional<Status> status = Optional.of(new Status("name", "short_"));
        when(mockStatusRepository.findById(0)).thenReturn(status);

        // Run the test
        final Optional<Status> result = statusServiceUnderTest.findById(0);

        // Verify the results
    }

    @Test
    void testFindAll() {
        // Setup

        // Configure StatusRepository.findAll(...).
        final Iterable<Status> statuses = Arrays.asList(new Status("name", "short_"));
        when(mockStatusRepository.findAll()).thenReturn(statuses);

        // Run the test
        final Iterable<Status> result = statusServiceUnderTest.findAll();

        // Verify the results
    }

    @Test
    void testDeleteById() {
        // Setup

        // Run the test
        statusServiceUnderTest.deleteById(0);

        // Verify the results
        verify(mockStatusRepository).deleteById(0);
    }
}
