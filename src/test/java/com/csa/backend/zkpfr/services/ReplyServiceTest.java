package com.csa.backend.zkpfr.services;

import com.csa.backend.zkpfr.model.*;
import com.csa.backend.zkpfr.repositories.ReplyRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;

import java.sql.Timestamp;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.mockito.MockitoAnnotations.initMocks;

class ReplyServiceTest {

    @Mock
    private ReplyRepository mockReplyRepository;
    @Mock
    private PostService mockPostService;

    @InjectMocks
    private ReplyService replyServiceUnderTest;

    @BeforeEach
    void setUp() {
        initMocks(this);
    }

    @Test
    void testGetReplyById() {
        // Setup

        // Configure ReplyRepository.findById(...).
        final Reply reply1 = new Reply();
        reply1.setId(0);
        reply1.setPost(new Post(new Timestamp(0L), new User("displayName", "email", "passHash", "aboutMe"), "title", "body", new Channel("name"), new Status("name", "short_")));
        reply1.setReplier(new User("displayName", "email", "passHash", "aboutMe"));
        reply1.setText("text");
        reply1.setLikes(0);
        reply1.setCreationDate(new Timestamp(0L));
        final Optional<Reply> reply = Optional.of(reply1);
        when(mockReplyRepository.findById(0)).thenReturn(reply);

        // Run the test
        final Optional<Reply> result = replyServiceUnderTest.getReplyById(0);

        // Verify the results
    }

    @Test
    void testCreateOrUpdate() {
        // Setup
        final Reply reply = new Reply();
        reply.setId(0);
        reply.setPost(new Post(new Timestamp(0L), new User("displayName", "email", "passHash", "aboutMe"), "title", "body", new Channel("name"), new Status("name", "short_")));
        reply.setReplier(new User("displayName", "email", "passHash", "aboutMe"));
        reply.setText("text");
        reply.setLikes(0);
        reply.setCreationDate(new Timestamp(0L));

        // Configure ReplyRepository.save(...).
        final Reply reply1 = new Reply();
        reply1.setId(0);
        reply1.setPost(new Post(new Timestamp(0L), new User("displayName", "email", "passHash", "aboutMe"), "title", "body", new Channel("name"), new Status("name", "short_")));
        reply1.setReplier(new User("displayName", "email", "passHash", "aboutMe"));
        reply1.setText("text");
        reply1.setLikes(0);
        reply1.setCreationDate(new Timestamp(0L));
        when(mockReplyRepository.save(any(Reply.class))).thenReturn(reply1);

        // Run the test
        final Reply result = replyServiceUnderTest.createOrUpdate(reply);

        // Verify the results
    }

    @Test
    void testDeleteReplyById() {
        // Setup

        // Run the test
        replyServiceUnderTest.deleteReplyById(0);

        // Verify the results
        verify(mockReplyRepository).deleteById(0);
    }

    @Test
    void testGetAllReplies() {
        // Setup

        // Configure ReplyRepository.findAll(...).
        final Reply reply = new Reply();
        reply.setId(0);
        reply.setPost(new Post(new Timestamp(0L), new User("displayName", "email", "passHash", "aboutMe"), "title", "body", new Channel("name"), new Status("name", "short_")));
        reply.setReplier(new User("displayName", "email", "passHash", "aboutMe"));
        reply.setText("text");
        reply.setLikes(0);
        reply.setCreationDate(new Timestamp(0L));
        final Iterable<Reply> replies = Arrays.asList(reply);
        when(mockReplyRepository.findAll()).thenReturn(replies);

        // Run the test
        final Iterable<Reply> result = replyServiceUnderTest.getAllReplies();

        // Verify the results
    }

    @Test
    void testUpVoteReply() {
        // Setup

        // Configure ReplyRepository.findById(...).
        final Reply reply1 = new Reply();
        reply1.setId(0);
        reply1.setPost(new Post(new Timestamp(0L), new User("displayName", "email", "passHash", "aboutMe"), "title", "body", new Channel("name"), new Status("name", "short_")));
        reply1.setReplier(new User("displayName", "email", "passHash", "aboutMe"));
        reply1.setText("text");
        reply1.setLikes(0);
        reply1.setCreationDate(new Timestamp(0L));
        final Optional<Reply> reply = Optional.of(reply1);
        when(mockReplyRepository.findById(0)).thenReturn(reply);

        // Configure ReplyRepository.save(...).
        final Reply reply2 = new Reply();
        reply2.setId(0);
        reply2.setPost(new Post(new Timestamp(0L), new User("displayName", "email", "passHash", "aboutMe"), "title", "body", new Channel("name"), new Status("name", "short_")));
        reply2.setReplier(new User("displayName", "email", "passHash", "aboutMe"));
        reply2.setText("text");
        reply2.setLikes(0);
        reply2.setCreationDate(new Timestamp(0L));
        when(mockReplyRepository.save(any(Reply.class))).thenReturn(reply2);

        // Run the test
        replyServiceUnderTest.upVoteReply(0);

        // Verify the results
    }

    @Test
    void testDownVoteReply() {
        // Setup

        // Configure ReplyRepository.findById(...).
        final Reply reply1 = new Reply();
        reply1.setId(0);
        reply1.setPost(new Post(new Timestamp(0L), new User("displayName", "email", "passHash", "aboutMe"), "title", "body", new Channel("name"), new Status("name", "short_")));
        reply1.setReplier(new User("displayName", "email", "passHash", "aboutMe"));
        reply1.setText("text");
        reply1.setLikes(0);
        reply1.setCreationDate(new Timestamp(0L));
        final Optional<Reply> reply = Optional.of(reply1);
        when(mockReplyRepository.findById(0)).thenReturn(reply);

        // Configure ReplyRepository.save(...).
        final Reply reply2 = new Reply();
        reply2.setId(0);
        reply2.setPost(new Post(new Timestamp(0L), new User("displayName", "email", "passHash", "aboutMe"), "title", "body", new Channel("name"), new Status("name", "short_")));
        reply2.setReplier(new User("displayName", "email", "passHash", "aboutMe"));
        reply2.setText("text");
        reply2.setLikes(0);
        reply2.setCreationDate(new Timestamp(0L));
        when(mockReplyRepository.save(any(Reply.class))).thenReturn(reply2);

        // Run the test
        replyServiceUnderTest.downVoteReply(0);

        // Verify the results
    }

    @Test
    void testGetRepliesByPostId() {
        // Setup

        // Configure ReplyRepository.findAllByPost(...).
        final Reply reply = new Reply();
        reply.setId(0);
        reply.setPost(new Post(new Timestamp(0L), new User("displayName", "email", "passHash", "aboutMe"), "title", "body", new Channel("name"), new Status("name", "short_")));
        reply.setReplier(new User("displayName", "email", "passHash", "aboutMe"));
        reply.setText("text");
        reply.setLikes(0);
        reply.setCreationDate(new Timestamp(0L));
        final List<Reply> replies = Arrays.asList(reply);
        when(mockReplyRepository.findAllByPost(any(Post.class))).thenReturn(replies);

        // Configure PostService.findById(...).
        final Optional<Post> post = Optional.of(new Post(new Timestamp(0L), new User("displayName", "email", "passHash", "aboutMe"), "title", "body", new Channel("name"), new Status("name", "short_")));
        when(mockPostService.findById(0)).thenReturn(post);

        // Run the test
        final Iterable<Reply> result = replyServiceUnderTest.getRepliesByPostId(0);

        // Verify the results
    }
}
