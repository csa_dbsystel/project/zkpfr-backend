package com.csa.backend.zkpfr.controller;

import com.csa.backend.zkpfr.model.Status;
import com.csa.backend.zkpfr.services.IStatusService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;

import java.util.Arrays;
import java.util.Optional;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.mockito.MockitoAnnotations.initMocks;

class StatusControllerTest {

    @Mock
    private IStatusService mockStatusService;

    private StatusController statusControllerUnderTest;

    @BeforeEach
    void setUp() {
        initMocks(this);
        statusControllerUnderTest = new StatusController(mockStatusService);
    }

    @Test
    void testAddStatus() {
        // Setup
        final Status statusToAdd = new Status("name", "short_");
        when(mockStatusService.save(any(Status.class))).thenReturn(new Status("name", "short_"));

        // Run the test
        final Status result = statusControllerUnderTest.addStatus(statusToAdd);

        // Verify the results
    }

    @Test
    void testUpdateStatus() {
        // Setup
        final Status statusToAdd = new Status("name", "short_");
        when(mockStatusService.save(any(Status.class))).thenReturn(new Status("name", "short_"));

        // Run the test
        final Status result = statusControllerUnderTest.updateStatus(statusToAdd);

        // Verify the results
    }

    @Test
    void testGetAllStatus() {
        // Setup

        // Configure IStatusService.findAll(...).
        final Iterable<Status> statuses = Arrays.asList(new Status("name", "short_"));
        when(mockStatusService.findAll()).thenReturn(statuses);

        // Run the test
        final Iterable<Status> result = statusControllerUnderTest.getAllStatus();

        // Verify the results
    }

    @Test
    void testGetStatusById() {
        // Setup

        // Configure IStatusService.findById(...).
        final Optional<Status> status = Optional.of(new Status("name", "short_"));
        when(mockStatusService.findById(0)).thenReturn(status);

        // Run the test
        final Status result = statusControllerUnderTest.getStatusById(0);

        // Verify the results
    }

    @Test
    void testDeleteStatusById() {
        // Setup

        // Run the test
        statusControllerUnderTest.deleteStatusById(0);

        // Verify the results
        verify(mockStatusService).deleteById(0);
    }
}
