package com.csa.backend.zkpfr.controller;

import com.csa.backend.zkpfr.model.Channel;
import com.csa.backend.zkpfr.services.IChannelService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;

import java.util.Arrays;
import java.util.Optional;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.mockito.MockitoAnnotations.initMocks;

class ChannelControllerTest {

    @Mock
    private IChannelService mockChannelService;

    private ChannelController channelControllerUnderTest;

    @BeforeEach
    void setUp() {
        initMocks(this);
        channelControllerUnderTest = new ChannelController(mockChannelService);
    }

    @Test
    void testAddChannel() {
        // Setup
        final Channel channelToAdd = new Channel("name");
        when(mockChannelService.save(any(Channel.class))).thenReturn(new Channel("name"));

        // Run the test
        final Channel result = channelControllerUnderTest.addChannel(channelToAdd);

        // Verify the results
    }

    @Test
    void testUpdateChannel() {
        // Setup
        final Channel channelToAdd = new Channel("name");
        when(mockChannelService.save(any(Channel.class))).thenReturn(new Channel("name"));

        // Run the test
        final Channel result = channelControllerUnderTest.updateChannel(channelToAdd);

        // Verify the results
    }

    @Test
    void testGetAllChannels() {
        // Setup
        when(mockChannelService.findAll()).thenReturn(Arrays.asList(new Channel("name")));

        // Run the test
        final Iterable<Channel> result = channelControllerUnderTest.getAllChannels();

        // Verify the results
    }

    @Test
    void testGetChannelById() {
        // Setup
        when(mockChannelService.findById(0)).thenReturn(Optional.of(new Channel("name")));

        // Run the test
        final Channel result = channelControllerUnderTest.getChannelById(0);

        // Verify the results
    }

    @Test
    void testDeletePostById() {
        // Setup

        // Run the test
        channelControllerUnderTest.deletePostById(0);

        // Verify the results
        verify(mockChannelService).deleteById(0);
    }
}
