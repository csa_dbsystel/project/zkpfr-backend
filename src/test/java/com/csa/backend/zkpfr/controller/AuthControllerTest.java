package com.csa.backend.zkpfr.controller;

import com.csa.backend.zkpfr.model.User;
import com.csa.backend.zkpfr.model.UserCredential;
import com.csa.backend.zkpfr.services.IUserService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;

import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.mockito.MockitoAnnotations.initMocks;

class AuthControllerTest {

    @Mock
    private IUserService mockUserService;

    private AuthController authControllerUnderTest;

    @BeforeEach
    void setUp() {
        initMocks(this);
        authControllerUnderTest = new AuthController(mockUserService);
    }

    @Test
    void testEmailIsAvailable() {
        // Setup
        when(mockUserService.emailIsAvailable("email")).thenReturn(true);

        // Run the test
        final boolean result = authControllerUnderTest.emailIsAvailable("email");

        // Verify the results
        assertTrue(result);
    }

    @Test
    void testUsernameIsAvailable() {
        // Setup
        when(mockUserService.usernameIsAvailable("username")).thenReturn(true);

        // Run the test
        final boolean result = authControllerUnderTest.usernameIsAvailable("username");

        // Verify the results
        assertTrue(result);
    }

    @Test
    void testRegisterNewUser() {
        // Setup
        final UserCredential userCredential = new UserCredential("email", "password", "displayName");

        // Configure IUserService.registerNewUser(...).
        final User user = new User("displayName", "email", "passHash", "aboutMe");
        when(mockUserService.registerNewUser(any(UserCredential.class))).thenReturn(user);

        // Run the test
        final User result = authControllerUnderTest.registerNewUser(userCredential);

        // Verify the results
    }

    @Test
    void testAuthenticateUser() {
        // Setup
        final UserCredential userCredential = new UserCredential("email", "password", "displayName");

        // Configure IUserService.getUserIfCredentialsMatch(...).
        final User user = new User("displayName", "email", "passHash", "aboutMe");
        when(mockUserService.getUserIfCredentialsMatch(any(UserCredential.class))).thenReturn(user);

        // Run the test
        final User result = authControllerUnderTest.authenticateUser(userCredential);

        // Verify the results
        verify(mockUserService).updateUserLastAccess(0);
    }
}
