package com.csa.backend.zkpfr.controller;

import com.csa.backend.zkpfr.model.*;
import com.csa.backend.zkpfr.services.IReplyService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;

import java.sql.Timestamp;
import java.util.Arrays;
import java.util.Optional;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.mockito.MockitoAnnotations.initMocks;

class ReplyControllerTest {

    @Mock
    private IReplyService mockReplyService;

    private ReplyController replyControllerUnderTest;

    @BeforeEach
    void setUp() {
        initMocks(this);
        replyControllerUnderTest = new ReplyController(mockReplyService);
    }

    @Test
    void testGetReplyById() {
        // Setup

        // Configure IReplyService.getReplyById(...).
        final Reply reply1 = new Reply();
        reply1.setId(0);
        reply1.setPost(new Post(new Timestamp(0L), new User("displayName", "email", "passHash", "aboutMe"), "title", "body", new Channel("name"), new Status("name", "short_")));
        reply1.setReplier(new User("displayName", "email", "passHash", "aboutMe"));
        reply1.setText("text");
        reply1.setLikes(0);
        reply1.setCreationDate(new Timestamp(0L));
        final Optional<Reply> reply = Optional.of(reply1);
        when(mockReplyService.getReplyById(0)).thenReturn(reply);

        // Run the test
        final Reply result = replyControllerUnderTest.getReplyById(0);

        // Verify the results
    }

    @Test
    void testAddReply() {
        // Setup
        final Reply replyToAdd = new Reply();
        replyToAdd.setId(0);
        replyToAdd.setPost(new Post(new Timestamp(0L), new User("displayName", "email", "passHash", "aboutMe"), "title", "body", new Channel("name"), new Status("name", "short_")));
        replyToAdd.setReplier(new User("displayName", "email", "passHash", "aboutMe"));
        replyToAdd.setText("text");
        replyToAdd.setLikes(0);
        replyToAdd.setCreationDate(new Timestamp(0L));

        // Configure IReplyService.createOrUpdate(...).
        final Reply reply = new Reply();
        reply.setId(0);
        reply.setPost(new Post(new Timestamp(0L), new User("displayName", "email", "passHash", "aboutMe"), "title", "body", new Channel("name"), new Status("name", "short_")));
        reply.setReplier(new User("displayName", "email", "passHash", "aboutMe"));
        reply.setText("text");
        reply.setLikes(0);
        reply.setCreationDate(new Timestamp(0L));
        when(mockReplyService.createOrUpdate(any(Reply.class))).thenReturn(reply);

        // Run the test
        final Reply result = replyControllerUnderTest.addReply(replyToAdd);

        // Verify the results
    }

    @Test
    void testUpdateReply() {
        // Setup
        final Reply replyToUpdate = new Reply();
        replyToUpdate.setId(0);
        replyToUpdate.setPost(new Post(new Timestamp(0L), new User("displayName", "email", "passHash", "aboutMe"), "title", "body", new Channel("name"), new Status("name", "short_")));
        replyToUpdate.setReplier(new User("displayName", "email", "passHash", "aboutMe"));
        replyToUpdate.setText("text");
        replyToUpdate.setLikes(0);
        replyToUpdate.setCreationDate(new Timestamp(0L));

        // Configure IReplyService.createOrUpdate(...).
        final Reply reply = new Reply();
        reply.setId(0);
        reply.setPost(new Post(new Timestamp(0L), new User("displayName", "email", "passHash", "aboutMe"), "title", "body", new Channel("name"), new Status("name", "short_")));
        reply.setReplier(new User("displayName", "email", "passHash", "aboutMe"));
        reply.setText("text");
        reply.setLikes(0);
        reply.setCreationDate(new Timestamp(0L));
        when(mockReplyService.createOrUpdate(any(Reply.class))).thenReturn(reply);

        // Run the test
        final Reply result = replyControllerUnderTest.updateReply(replyToUpdate);

        // Verify the results
    }

    @Test
    void testGetAllReplies() {
        // Setup

        // Configure IReplyService.getAllReplies(...).
        final Reply reply = new Reply();
        reply.setId(0);
        reply.setPost(new Post(new Timestamp(0L), new User("displayName", "email", "passHash", "aboutMe"), "title", "body", new Channel("name"), new Status("name", "short_")));
        reply.setReplier(new User("displayName", "email", "passHash", "aboutMe"));
        reply.setText("text");
        reply.setLikes(0);
        reply.setCreationDate(new Timestamp(0L));
        final Iterable<Reply> replies = Arrays.asList(reply);
        when(mockReplyService.getAllReplies()).thenReturn(replies);

        // Run the test
        final Iterable<Reply> result = replyControllerUnderTest.getAllReplies();

        // Verify the results
    }

    @Test
    void testDeleteReplyById() {
        // Setup

        // Run the test
        replyControllerUnderTest.deleteReplyById(0);

        // Verify the results
        verify(mockReplyService).deleteReplyById(0);
    }

    @Test
    void testUpVoteReplyById() {
        // Setup

        // Run the test
        replyControllerUnderTest.upVoteReplyById(0);

        // Verify the results
        verify(mockReplyService).upVoteReply(0);
    }

    @Test
    void testDownVoteReplyById() {
        // Setup

        // Run the test
        replyControllerUnderTest.downVoteReplyById(0);

        // Verify the results
        verify(mockReplyService).downVoteReply(0);
    }

    @Test
    void testGetRepliesByPostId() {
        // Setup

        // Configure IReplyService.getRepliesByPostId(...).
        final Reply reply = new Reply();
        reply.setId(0);
        reply.setPost(new Post(new Timestamp(0L), new User("displayName", "email", "passHash", "aboutMe"), "title", "body", new Channel("name"), new Status("name", "short_")));
        reply.setReplier(new User("displayName", "email", "passHash", "aboutMe"));
        reply.setText("text");
        reply.setLikes(0);
        reply.setCreationDate(new Timestamp(0L));
        final Iterable<Reply> replies = Arrays.asList(reply);
        when(mockReplyService.getRepliesByPostId(0)).thenReturn(replies);

        // Run the test
        final Iterable<Reply> result = replyControllerUnderTest.getRepliesByPostId(0);

        // Verify the results
    }
}
