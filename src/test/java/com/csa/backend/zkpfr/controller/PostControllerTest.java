package com.csa.backend.zkpfr.controller;

import com.csa.backend.zkpfr.model.Channel;
import com.csa.backend.zkpfr.model.Post;
import com.csa.backend.zkpfr.model.Status;
import com.csa.backend.zkpfr.model.User;
import com.csa.backend.zkpfr.services.IPostService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;

import java.sql.Timestamp;
import java.util.Arrays;
import java.util.Optional;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.mockito.MockitoAnnotations.initMocks;

class PostControllerTest {

    @Mock
    private IPostService mockPostService;

    private PostController postControllerUnderTest;

    @BeforeEach
    void setUp() {
        initMocks(this);
        postControllerUnderTest = new PostController(mockPostService);
    }

    @Test
    void testAddPost() {
        // Setup
        final Post postToAdd = new Post(new Timestamp(0L), new User("displayName", "email", "passHash", "aboutMe"), "title", "body", new Channel("name"), new Status("name", "short_"));

        // Configure IPostService.save(...).
        final Post post = new Post(new Timestamp(0L), new User("displayName", "email", "passHash", "aboutMe"), "title", "body", new Channel("name"), new Status("name", "short_"));
        when(mockPostService.save(any(Post.class))).thenReturn(post);

        // Run the test
        final Post result = postControllerUnderTest.addPost(postToAdd);

        // Verify the results
    }

    @Test
    void testUpdatePost() {
        // Setup
        final Post postToAdd = new Post(new Timestamp(0L), new User("displayName", "email", "passHash", "aboutMe"), "title", "body", new Channel("name"), new Status("name", "short_"));

        // Configure IPostService.save(...).
        final Post post = new Post(new Timestamp(0L), new User("displayName", "email", "passHash", "aboutMe"), "title", "body", new Channel("name"), new Status("name", "short_"));
        when(mockPostService.save(any(Post.class))).thenReturn(post);

        // Run the test
        final Post result = postControllerUnderTest.updatePost(postToAdd);

        // Verify the results
    }

    @Test
    void testGetAllPosts() {
        // Setup

        // Configure IPostService.findAll(...).
        final Iterable<Post> posts = Arrays.asList(new Post(new Timestamp(0L), new User("displayName", "email", "passHash", "aboutMe"), "title", "body", new Channel("name"), new Status("name", "short_")));
        when(mockPostService.findAll()).thenReturn(posts);

        // Run the test
        final Iterable<Post> result = postControllerUnderTest.getAllPosts();

        // Verify the results
    }

    @Test
    void testGetPostById() {
        // Setup

        // Configure IPostService.findById(...).
        final Optional<Post> post = Optional.of(new Post(new Timestamp(0L), new User("displayName", "email", "passHash", "aboutMe"), "title", "body", new Channel("name"), new Status("name", "short_")));
        when(mockPostService.findById(0)).thenReturn(post);

        // Run the test
        final Post result = postControllerUnderTest.getPostById(0);

        // Verify the results
    }

    @Test
    void testDeletePostById() {
        // Setup

        // Run the test
        postControllerUnderTest.deletePostById(0);

        // Verify the results
        verify(mockPostService).deleteById(0);
    }

    @Test
    void testGetLatestFromAllPosts() {
        // Setup

        // Configure IPostService.getLatestPost(...).
        final Post post = new Post(new Timestamp(0L), new User("displayName", "email", "passHash", "aboutMe"), "title", "body", new Channel("name"), new Status("name", "short_"));
        when(mockPostService.getLatestPost()).thenReturn(post);

        // Run the test
        final Post result = postControllerUnderTest.getLatestFromAllPosts();

        // Verify the results
    }

    @Test
    void testGetAllPostsByChannelId() {
        // Setup

        // Configure IPostService.findAllByChannelId(...).
        final Iterable<Post> posts = Arrays.asList(new Post(new Timestamp(0L), new User("displayName", "email", "passHash", "aboutMe"), "title", "body", new Channel("name"), new Status("name", "short_")));
        when(mockPostService.findAllByChannelId(0)).thenReturn(posts);

        // Run the test
        final Iterable<Post> result = postControllerUnderTest.getAllPostsByChannelId(0);

        // Verify the results
    }
}
