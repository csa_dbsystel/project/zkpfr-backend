package com.csa.backend.zkpfr.controller;

import com.csa.backend.zkpfr.model.Tag;
import com.csa.backend.zkpfr.services.ITagService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;

import java.util.Arrays;
import java.util.Optional;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.mockito.MockitoAnnotations.initMocks;

class TagControllerTest {

    @Mock
    private ITagService mockTagService;

    private TagController tagControllerUnderTest;

    @BeforeEach
    void setUp() {
        initMocks(this);
        tagControllerUnderTest = new TagController(mockTagService);
    }

    @Test
    void testAddTag() {
        // Setup
        final Tag tagToAdd = new Tag(0, "tagName");
        when(mockTagService.save(any(Tag.class))).thenReturn(new Tag(0, "tagName"));

        // Run the test
        final Tag result = tagControllerUnderTest.addTag(tagToAdd);

        // Verify the results
    }

    @Test
    void testUpdateTag() {
        // Setup
        final Tag tagToAdd = new Tag(0, "tagName");
        when(mockTagService.save(any(Tag.class))).thenReturn(new Tag(0, "tagName"));

        // Run the test
        final Tag result = tagControllerUnderTest.updateTag(tagToAdd);

        // Verify the results
    }

    @Test
    void testGetAllTags() {
        // Setup
        when(mockTagService.findAll()).thenReturn(Arrays.asList(new Tag(0, "tagName")));

        // Run the test
        final Iterable<Tag> result = tagControllerUnderTest.getAllTags();

        // Verify the results
    }

    @Test
    void testGetTagById() {
        // Setup
        when(mockTagService.findById(0)).thenReturn(Optional.of(new Tag(0, "tagName")));

        // Run the test
        final Tag result = tagControllerUnderTest.getTagById(0);

        // Verify the results
    }

    @Test
    void testGetTagByPostId() {
        // Setup
        when(mockTagService.findByPostId(0)).thenReturn(Arrays.asList(new Tag(0, "tagName")));

        // Run the test
        final Iterable<Tag> result = tagControllerUnderTest.getTagByPostId(0);

        // Verify the results
    }

    @Test
    void testDeletePostById() {
        // Setup

        // Run the test
        tagControllerUnderTest.deletePostById(0);

        // Verify the results
        verify(mockTagService).deleteById(0);
    }
}
