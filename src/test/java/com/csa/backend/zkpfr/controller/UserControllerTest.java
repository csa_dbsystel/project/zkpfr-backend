package com.csa.backend.zkpfr.controller;

import com.csa.backend.zkpfr.model.User;
import com.csa.backend.zkpfr.services.IUserService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;

import java.util.Arrays;
import java.util.Optional;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.mockito.MockitoAnnotations.initMocks;

class UserControllerTest {

    @Mock
    private IUserService mockUserService;

    private UserController userControllerUnderTest;

    @BeforeEach
    void setUp() {
        initMocks(this);
        userControllerUnderTest = new UserController(mockUserService);
    }

    @Test
    void testGetAllUsers() {
        // Setup

        // Configure IUserService.getAllUsers(...).
        final Iterable<User> users = Arrays.asList(new User("displayName", "email", "passHash", "aboutMe"));
        when(mockUserService.getAllUsers()).thenReturn(users);

        // Run the test
        final Iterable<User> result = userControllerUnderTest.getAllUsers();

        // Verify the results
    }

    @Test
    void testAddUser() {
        // Setup
        final User userToAdd = new User("displayName", "email", "passHash", "aboutMe");

        // Configure IUserService.createUser(...).
        final User user = new User("displayName", "email", "passHash", "aboutMe");
        when(mockUserService.createUser(any(User.class))).thenReturn(user);

        // Run the test
        final User result = userControllerUnderTest.addUser(userToAdd);

        // Verify the results
    }

    @Test
    void testGetUserById() {
        // Setup

        // Configure IUserService.getUserById(...).
        final Optional<User> user = Optional.of(new User("displayName", "email", "passHash", "aboutMe"));
        when(mockUserService.getUserById(0)).thenReturn(user);

        // Run the test
        final User result = userControllerUnderTest.getUserById(0);

        // Verify the results
    }

    @Test
    void testUpdateUser() {
        // Setup
        final User userToUpdate = new User("displayName", "email", "passHash", "aboutMe");

        // Configure IUserService.updateUser(...).
        final User user = new User("displayName", "email", "passHash", "aboutMe");
        when(mockUserService.updateUser(any(User.class))).thenReturn(user);

        // Run the test
        final User result = userControllerUnderTest.updateUser(userToUpdate);

        // Verify the results
    }

    @Test
    void testDeleteUserById() {
        // Setup

        // Run the test
        userControllerUnderTest.deleteUserById(0);

        // Verify the results
        verify(mockUserService).deleteUserById(0);
    }
}
